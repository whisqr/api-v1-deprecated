<h1>Membership/Loyalty API Test</h1>

<?php

require_once('Whisqr.php');

// Get your public and private keys from your Whisqr admin console and replace the placeholders below.
$membership = new \Whisqr\Whisqr(
	'sk_live_fbbcf934aa37925b5fbee7ba8058d563d1efa1323aaaa677abfcdaa05a10f648', 
	'pk_live_55e72d0b5ee38bfaf1595389fb744e914d8b492d02e27f4fdb44f490d50f88f2'
);


$emailTest = 'emailtest@domain.com';
$cardcodeTest = '5TJ2Kx';

// Check to see if email matches account with active membership

?>

<h2>Testing using email:</h2>
<?php
echo $emailTest;
?>

<h2>Testing using cardcode:</h2>
<?php
echo $cardcodeTest;
?>

<h2>Generate API keys for Mobile App:</h2>
Result: <?php
$keysGenerated = $membership->generateAppkeys();
echo json_encode($keysGenerated);
?>

<h2>Delete API key for Mobile App:</h2>
Result: <?php
echo json_encode($membership->deleteAppkeys($keysGenerated['keyPublic']));

?>

<h2>Membership Test:</h2>
<?php
echo ($membership->checkActive($emailTest)) ? ('membership is active') : ('membership is not active');
?>

<h2>Card Punch Test (email):</h2>
Result: <?php
echo json_encode($membership->punchEmail($emailTest));
?>

<h2>Card Punch Test (card code):</h2>
Result: <?php
echo json_encode($membership->punchCardcode($cardcodeTest));

?>
<h2>Get Current Punch Total (email):</h2>
Result: <?php
echo json_encode($membership->getPunchtotalEmail($emailTest));

?>
<h2>Get Current Punch Total (cardcode):</h2>
Result: <?php
echo json_encode($membership->getPunchtotalCardcode($cardcodeTest));
