<?php

namespace Whisqr;

// Use this class to simplify API calls to Whisqr Loyalty and Membership Programs

class Whisqr
{
	protected $hashSecret;
	protected $hashPublic;

	public function __construct($hashSecret, $hashPublic) {
		$this->hashSecret = $hashSecret;
		$this->hashPublic = $hashPublic;
	}

	// Generate app key pair for use with Mobile App / web app / POS System
	public function generateAppkeys() {

		$headers = array(
			'X-Public: ' . $this->hashPublic,
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/apikeys');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

		    throw new MyException('Error querying API', 0, $e);

		}

	}

	// Delete app key pair when they need to be retired/when they have been compromised.
	// Only app key pairs can be deleted using the API; and only when using administrator key pair.
	public function deleteAppkeys($keyPublicDelete) {

		$content = json_encode(array(
			'keyPublicDelete' => $keyPublicDelete
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/apikeys');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);
			curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "DELETE");

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

		    throw new MyException('Error querying API', 0, $e);

		}

	}

	// Check to see if a user has an active membership (using email address)
	public function checkActive($email) {

		$content = json_encode(array(
			'email' => $email
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/ismember');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			if(isset($result['status']) && $result['status'] == 'success') {

				return true;

			} else {

				return false;

			}

		} catch (Exception $e) {

		    throw new MyException('Error querying API', 0, $e);

		}

	}

	// Punch a user's card (using email address)
	public function punchEmail($email) {

		$content = json_encode(array(
			'email' => $email
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/punch');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

			throw new MyException('Error querying API', 0, $e);

		}

	}

	// Punch a user's card (using card code)
	public function punchCardcode($cardcode) {

		$content = json_encode(array(
			'cardcode' => $cardcode
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/punch');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

			throw new MyException('Error querying API', 0, $e);

		}

	}

	// get a member's punchtotal (using email address)
	public function getPunchtotalEmail($email) {

		$content = json_encode(array(
			'email' => $email
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/punchtotal');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

		    throw new MyException('Error querying API', 0, $e);

		}

	}

	// get a member's punchtotal (using email address)
	public function getPunchtotalCardcode($cardcode) {

		$content = json_encode(array(
			'cardcode' => $cardcode
		));

		$contentHash = hash_hmac('sha256', $content, explode('_', $this->hashSecret)[2]);

		$headers = array(
			'X-Public: ' . $this->hashPublic,
			'X-Hash: ' . $contentHash
		);

		try {

			$ch = curl_init('https://whisqr.com/api/v1/punchtotal');
			curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$content);

			$result = json_decode(curl_exec($ch), true);
			curl_close($ch);

			return $result;

		} catch (Exception $e) {

		    throw new MyException('Error querying API', 0, $e);

		}

	}



}